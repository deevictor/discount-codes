# README #

app/api/coupons.py - View logic <br />
app/schemas/coupon.py - Data validation <br />
app/tests/test_coupons.py - Tests <br />

### How to run using docker ###
`$ make build`

### To run flake8, tests and typing check ###
`$ make check`

### Docs

[http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)

[http://127.0.0.1:8000/redoc](http://127.0.0.1:8000/redoc)
