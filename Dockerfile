FROM python:3.8

WORKDIR /app

COPY requirements.txt /tmp/
COPY requirements_dev.txt /tmp/

RUN pip3 install -r /tmp/requirements_dev.txt --no-cache-dir

EXPOSE 5432
CMD ["bash"]