SHELL := /bin/bash

docker_run := docker-compose run --rm
docker_run_backend := $(docker_run) discount_api

logs:
	docker logs -f --tail 1000 discount_api_container

shell:
	$(docker_run_backend) ipython3

build:
	docker-compose up -d --build

flake8:
	$(docker_run_backend) flake8

pytest:
	$(docker_run_backend) pytest

mypy:
	$(docker_run_backend) mypy .

check: flake8 pytest mypy