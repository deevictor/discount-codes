from sqlalchemy.ext.asyncio import AsyncSession

from app.database import async_session

__all__ = ['db_session']


async def db_session() -> AsyncSession:
    async with async_session() as session:
        try:
            yield session
            await session.commit()
        except Exception as e:
            await session.rollback()
            raise e
        finally:
            await session.close()
