from typing import List

from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from app.dependencies import db_session
from app.models import Coupon, CouponStatus
from app.schemas import coupon as coupon_schemas

router = APIRouter()


@router.put("/{brand_id}", response_model=coupon_schemas.CouponResponse)
async def get_coupon(brand_id: str,
                     coupon_in: coupon_schemas.CouponUpdate,
                     session: AsyncSession = Depends(db_session)) -> coupon_schemas.CouponResponse:
    """
    Update existing coupon.
    """
    statement = select(Coupon).filter_by(brand_id=brand_id, status=CouponStatus.available)
    result = await session.execute(statement=statement)
    coupon = result.scalars().first()
    if not coupon:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'no available coupons of {brand_id} is found')
    update_data = coupon_in.dict()
    setattr(coupon, 'status', CouponStatus.taken)
    setattr(coupon, 'user_id', update_data.get("user_id"))
    session.add(coupon)
    await session.commit()
    await session.refresh(coupon)
    return coupon_schemas.CouponResponse(code=coupon.code)


@router.post("/coupons", response_model=List[coupon_schemas.CouponResponse], status_code=status.HTTP_201_CREATED)
async def create_coupons(coupon_in: coupon_schemas.CouponsCreate,
                         session: AsyncSession = Depends(db_session)) -> List[coupon_schemas.CouponResponse]:
    """
    Create certain amount of coupons of certain brand.
    """
    obj_in_data = coupon_in.dict()
    amount = obj_in_data.get("amount", None)
    brand_id = obj_in_data.get("brand_id")
    coupons = []
    for i in range(amount):
        new_coupon = Coupon(brand_id=brand_id)
        coupons.append(new_coupon)
    session.add_all(coupons)
    await session.commit()
    return_codes = [coupon_schemas.CouponResponse(code=coupon.code) for coupon in coupons]
    return return_codes
