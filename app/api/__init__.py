from fastapi import APIRouter

from app.api import coupons

__all__ = ['api_router']

common_prefix = "/api"
api_router = APIRouter()
api_router.include_router(coupons.router, prefix=f"{common_prefix}/discount", tags=["discount"])
