from pydantic import BaseModel, Field


class CouponResponse(BaseModel):
    code: str = Field(max_length=36, example="88f4148a-c9f4-4694-a826-383857f3490e", description="Code of the coupon")

    class Config:
        orm_mode = True


class CouponUpdate(BaseModel):
    user_id: str = Field(max_length=20, example="user1", description="user_id of the user who receives the coupon.")


class CouponsCreate(BaseModel):
    brand_id: str = Field(max_length=50, example="brand1", description="brand ID value.")
    amount: int = Field(gt=0, lt=1000, example=5, description="number of codes to generate for the brand.")
