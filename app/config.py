from pydantic import BaseSettings

__all__ = ['settings']


class Settings(BaseSettings):
    DB_ECHO: bool = True
    DB: str = 'postgresql+asyncpg://postgres:postgres@db:5432/discount'
    TEST_DB: str = 'postgresql+asyncpg://postgres:postgres@db-test:5432/discount_test'
    BASE_URL: str = 'http://127.0.0.1:8000/api/discount'
    TEST_USER_ID: str = 'test_user'
    TEST_BRAND: str = 'test_brand'


settings = Settings()
