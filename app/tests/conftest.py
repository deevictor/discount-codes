import logging
from contextlib import asynccontextmanager

import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from app.config import settings
from app.database import Base
from app.dependencies import db_session as app_db_session
from app.main import app


logger = logging.getLogger(__name__)


@pytest.fixture(scope='function')
async def session_client(create_db_session):
    async def _db_session():
        # create a db session in order to commit on request end
        async with create_db_session() as session:
            yield session

    # override session per
    old_overrides = app.dependency_overrides
    try:
        app.dependency_overrides[app_db_session] = _db_session
        async with AsyncClient(app=app, base_url=settings.BASE_URL) as client:
            yield client
    except Exception as e:
        logger.debug(e)
    finally:
        app.dependency_overrides = old_overrides


@pytest.fixture(scope='function')
async def db_session(create_db_session):
    """Fixture which provides active db session"""
    async with create_db_session() as session:
        yield session


@pytest.fixture(scope='function')
async def create_db_session():
    """Fixture for creating new db sessions"""
    engine = create_async_engine(settings.TEST_DB)
    async with engine.begin() as con:
        await con.run_sync(Base.metadata.drop_all)
        await con.run_sync(Base.metadata.create_all)

    create_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)

    @asynccontextmanager
    async def session_scope():
        async with create_session() as session:
            try:
                yield session
                await session.commit()
            except:  # noqa: B901
                await session.rollback()
                raise

    yield session_scope

    async with engine.begin() as con:
        await con.run_sync(Base.metadata.drop_all)
