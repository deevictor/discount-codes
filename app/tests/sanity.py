import pytest


def test_sanity():
    assert 200 == 200


@pytest.mark.xfail
def test_sanity_fail():
    assert 200 != 200


@pytest.mark.skip
def test_sanity_skip():
    assert 200 != 200
