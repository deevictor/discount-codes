import uuid
from datetime import datetime

import factory

from app.config import settings
from app.models import Coupon, CouponStatus


class SQLAlchemyFactory(factory.Factory):
    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        async def _create(db_session):
            obj = model_class(*args, **kwargs)
            db_session.add(obj)
            await db_session.commit()
            return obj
        return _create


def uuid_sequence():
    return factory.Sequence(lambda _: str(uuid.uuid4()))


class CouponFactory(SQLAlchemyFactory):
    class Meta:
        model = Coupon

    id = factory.Sequence(lambda n: n)
    code = uuid_sequence()
    user_id = None
    status = CouponStatus.available
    brand_id = settings.TEST_BRAND
    created_at = factory.Sequence(lambda _: datetime.now())
