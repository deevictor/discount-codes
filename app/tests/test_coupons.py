import re
import string
import random
from random import randrange

import pytest
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.config import settings
from app.schemas.coupon import CouponResponse
from app.tests.factories import CouponFactory


def valid_uuid(uuid):
    regex = re.compile(r'^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z', re.I)
    match = regex.match(uuid)
    return bool(match)


@pytest.mark.asyncio
async def test_create_coupons(session_client: AsyncClient) -> None:
    coupons_amount = randrange(10)
    random_brand_coupons = {
        "brand_id": ''.join(random.choices(string.ascii_letters + string.digits, k=10)),
        "amount": coupons_amount
    }
    response = await session_client.post("/coupons", json=random_brand_coupons)
    code_responses = response.json()

    assert response.status_code == 201
    for code_response in code_responses:
        assert valid_uuid(code_response.get('code', None))
    assert len(code_responses) == coupons_amount


@pytest.mark.asyncio
async def test_retrieve_single_code(session_client: AsyncClient, db_session: AsyncSession) -> None:
    coupon = await CouponFactory()(db_session)
    brand_id = settings.TEST_BRAND
    test_user_id = {'user_id': settings.TEST_USER_ID}
    response = await session_client.put(f"/{brand_id}", json=test_user_id)
    code_response = response.json()
    coupon = CouponResponse.from_orm(coupon).dict(exclude_unset=True)

    assert response.status_code == 200
    assert code_response.get('code') == coupon.get('code')
    assert valid_uuid(code_response.get('code'))
