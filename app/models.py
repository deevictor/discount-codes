import enum
import logging
import uuid

from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    func,
    Enum)

from app.database import Base

__all__ = ['Coupon', 'CouponStatus']

logger = logging.getLogger(__name__)


class CouponStatus(str, enum.Enum):
    available = 'available'
    taken = 'taken'


class Coupon(Base):
    __tablename__ = 'coupons'

    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String, index=True, default=lambda: str(uuid.uuid4()), unique=True)
    user_id = Column(String, nullable=True, index=True)
    status = Column(Enum(CouponStatus, values_callable=lambda obj: [item.value for item in obj]),
                    default=CouponStatus.available.value, server_default=CouponStatus.available.value,
                    index=True, nullable=False)
    brand_id = Column(String, nullable=False, index=True)
    created_at = Column(DateTime, nullable=False, server_default=func.now())
