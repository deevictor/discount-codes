import logging
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.api import api_router

__all__ = ['app']

from app.logger.log_init import setup_logging

setup_logging()
logger = logging.getLogger(__name__)

app = FastAPI()
app.include_router(api_router)

origins = [
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
